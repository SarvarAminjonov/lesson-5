fetch('https://jsonplaceholder.typicode.com/users')
.then(fetchData => fetchData.json())
.then(data => console.log(data))

// =====================

async function asyncFunc(){
    try{
      let res = await fetch('https://jsonplaceholder.typicode.com/users');
      let data = await res.json();
      console.log('Data', data);
    }catch (e){
     console.error(e);
    }finally{
   
    }
   }
   
   asyncFunc()
   