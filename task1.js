console.log('Request data...');
let pro = new Promise((resolve,reject) => {
  setTimeout(() => {
    console.log('Preparing data...');
    const data = {
      server: 'aws',
      port: 2000,
      name: 'Sarvar'
    }
    resolve(data)
  }, 2000);

});

pro.then( backendData => {
  return new Promise((resolve,reject) => {
    setTimeout(() => {
      backendData.age = 27;
      resolve(backendData)
    }, 3000);
  })
  
  
}).then( (clientdata) => {
  console.log('Data received', clientdata);
})